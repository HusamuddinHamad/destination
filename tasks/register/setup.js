// Template Setup Task
module.exports = function (grunt) {
	grunt.registerTask('setup', function() {
		var tasks = [
			'jshint',
			'clean:all',
			'bower',
			'imagemin',
			'uglify',
			'htmlmin',
			'sass',
			'copy',
			'concat',
			'clean:cleanCSS'
		];
		return grunt.task.run(tasks);
	});
};