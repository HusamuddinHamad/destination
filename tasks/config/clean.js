module.exports = function (grunt) {
	var pipeline = require('../pipeline');
	var production = pipeline.production;

	grunt.config.set('clean', {
		all: {
			files: [{
				src: production.css.dir
			}, {
				src: production.fonts
			}, {
				src: production.js.dir
			}, {
				src: production.images.dir
			}, {
				src: production.templates.dir
			}]
		},
		cleanAllCSS: {
			files: [{
				src: production.css.dir
			}]
		},
		cleanCSS: {
			files: [{
				src: [
					production.css.vendor,
					production.css.file
				]
			}]
		},
		cleanImages: {
			files: [{
				src: production.images.dir
			}]
		},
		cleanJS: {
			files: [{
				src: production.js.dir
			}]
		},
		cleanTemplates: {
			files: [{
				src: production.templates.dir
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
};