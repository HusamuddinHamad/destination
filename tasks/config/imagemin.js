// Image min
module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('imagemin', {
		dist: {
			files: [{
				expand: true,
				cwd: resources.images.dir,
				src: ['**/*.{png,jpg,gif}'],
				dest: production.images.dir
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-imagemin');
};