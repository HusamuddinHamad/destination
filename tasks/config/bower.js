module.exports = function(grunt) {
	var pipeline = require('../pipeline');
	var production = pipeline.production;

    grunt.config.set('bower', {
        install: {
            options: {
                targetDir: production.libs,
                layout: 'byType',
                install: true,
                verbose: false,
                cleanTargetDir: false,
                cleanBowerDir: false,
            }
        }
    });

    grunt.loadNpmTasks('grunt-bower-task');
};
