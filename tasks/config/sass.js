// Dev and production build for sass
module.exports = function (grunt) {
	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('sass', {
		dist: {
			options: {
				sourcemap: 'none',
				style: 'expanded',
				noCache: true,
			},
			files: [{
				src: resources.sass.file,
				dest: production.css.file
			}, {
				src: resources.sass.vendor,
				dest: production.css.vendor
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
};