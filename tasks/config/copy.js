module.exports = function (grunt) {
	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('copy', {
		fonts: {
			files: [{
				expand: true,
				src: production.copy,
				dest: production.fonts,
				flatten: true,
				filter: 'isFile'
			}]
		},
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
};
