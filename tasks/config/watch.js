// Watches for changes and runs tasks
module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('watch', {

		grunt: {
			files: ['tasks/**/*.js', 'Gruntfile.js'],
			options: {
				livereload: true
			},
			tasks: ['jshint:grunt', 'setup', 'notify:grunt'],
		},

		sass: {
			files: [ resources.sass.dir + '**/*.scss' ],
			options: {
				livereload: false
			},
			tasks: ['clean:cleanAllCSS', 'sass', 'concat', 'clean:cleanCSS']
		},

		css: {
			files: [ production.css.dir + '**/*.css' ],
			options: {
				livereload: true
			},
			tasks: ['notify:css']
		},

		images: {
			files: [ resources.images.dir + '**' ],
			options: {
				livereload: true
			},
			tasks: ['clean:cleanImages', 'imagemin', 'notify:images']
		},

		js: {
			files: [resources.js.dir + '**/*.js'],
			options: {
				livereload: true
			},
			tasks: ['jshint:scripts', 'clean:cleanJS', 'uglify', 'notify:js'],
		},

		templates: {
			files: [resources.templates.dir + '**'],
			options: {
				livereload: true
			},
			tasks: ['clean:cleanTemplates', 'htmlmin', 'notify:templates']
		},

		html: {
			files: ['resources/views/**/*.blade.php'],
			options: {
				livereload: true
			},
			tasks: ['notify:html']
		}

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
};
