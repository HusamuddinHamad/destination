// JsHint your javascript
module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('jshint', {
		grunt : [
			'Gruntfile.js',
			'tasks/**/*.js',
		],
		scripts: [
			resources.js.dir + '**/*.js'
		],
		options : {
			browser: true,
			curly: false,
			eqeqeq: false,
			eqnull: true,
			expr: true,
			immed: true,
			newcap: true,
			noarg: true,
			smarttabs: true,
			sub: true,
			undef: false
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
};
