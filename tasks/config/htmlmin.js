// Image min
module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('htmlmin', {
		dist: {
			options: {
				removeComments: true,
				collapseWhitespace: true,
			},
			files: [{
				expand: true,
				cwd: resources.templates.dir,
				src: ['**/*.html'],
				dest: production.templates.dir
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-htmlmin');
};