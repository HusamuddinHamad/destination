module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;
	var resources = pipeline.resources;

	grunt.config.set('uglify', {
		dist: {
			options: {
				sourceMap: false,
				mangle: false,
				compress: false,
				beautify: true,
			},
			expand: true,
			cwd: resources.js.dir,
			src: ['**/*.js'],
			dest: production.js.dir
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');

};