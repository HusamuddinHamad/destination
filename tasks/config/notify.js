module.exports = function(grunt) {

	grunt.config.set('notify', {
		options: {
		    enabled: true,
		    max_jshint_notifications: 5,
		    success: false,
		    duration: 1,
		},
		watch: {
			options: {
				message: "Now, your watch begins"
			}
		},
		grunt: {
			options: {
				message: "grunt updated"
			}
		},
		sass: {
			options: {
				message: "sass updated"
			}
		},
		css: {
			options: {
				message: "css updated"
			}
		},
		js: {
			options: {
				message: "js updated"
			}
		},
		images: {
			options: {
				message: "images updated"
			}
		},
		html: {
			options: {
				message: "html updated"
			}
		},
		templates: {
			options: {
				message: "templates updated"
			}
		},
	});

	grunt.loadNpmTasks('grunt-notify');
};