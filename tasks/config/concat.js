// Dev and production build for sass
module.exports = function (grunt) {

	var pipeline = require('../pipeline');
	var production = pipeline.production;

	grunt.config.set('concat', {
		dist: {
			src: production.concat.css,
			dest: production.css.dest
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
};