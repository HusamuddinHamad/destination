var
	production = {},
	resources = {},
	paths = {
		resources: 'resources/assets/',
		production: 'public/',
		vendor: 'public/libs/'
	};

/* -------------
 * Production
 */

production.css = {
	dir: paths.production + 'css/',
	file: paths.production + 'css/style.css',
	dest: paths.production + 'css/app.css',
	vendor: paths.production + 'css/vendor.css',
};

production.templates = {
	dir: paths.production + 'templates/',
};

production.fonts = {
	dir: paths.production + 'fonts/',
};

production.js = {
	dir: paths.production + 'js/'
};

production.images = {
	dir: paths.production + 'images/'
};

production.libs = paths.production + 'libs/';

production.copy = [
	production.libs + 'bootstrap/dist/fonts/**',
	production.libs + 'fontawesome/fonts/**'
];

production.fonts = paths.production + 'fonts/';

production.concat = {};

production.concat.css = [
	production.css.vendor,
	paths.vendor + 'bootstrap/dist/css/bootstrap.css',
	paths.vendor + 'fontawesome/css/font-awesome.css',
	production.css.file
];

/* End production
 * -------------
 */

/* -------------
 * Resources
 */

resources.sass = {
	dir: paths.resources + "styles/",
	file: paths.resources + "styles/importer.scss",
	vendor: paths.resources + "styles/vendor.scss"
};

resources.images = {
	dir: paths.resources + "images/"
};

resources.js = {
	dir: paths.resources + "scripts/"
};

resources.templates = {
	dir: paths.resources + "templates/"
};

/* End Resources
 * -------------
 */

module.exports.paths = paths;
module.exports.production = production;
module.exports.resources = resources;