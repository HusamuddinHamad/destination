# Destination
Destination is Laravel 5 based Project

### Used Technologies
- Larave 5 Framework
- guzzle/guzzle HTTP library
- AWS SDK

### Installation Guide (you have to run the following command-lines)
- composer install
- php artisan migrate --seed