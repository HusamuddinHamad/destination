<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

class AttachmentFormRequest extends Request {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'	=> 'required',
			'type' 	=> 'required',
			'data'	=> 'required',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	protected function formatErrors(Validator $validator)
	{
		return [
			'success' 	=> false,
			'messages' 	=> $validator->errors()->all()
		];
	}

}
