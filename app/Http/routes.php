<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// User Routes
Route::controllers([
	'user' 			=> 'Auth\AuthController',
	'user/password' => 'Auth\PasswordController',
]);

// API Routes
Route::group(['prefix' => 'api', 'middleware' => 'auth'], function () {
	Route::resource(
		'project',
		'ProjectController',
		[
			'only' => [
				'index',
				'show',
				'store',
				'update',
				'destroy',
			]
		]
	);

	Route::resource(
		'attachment',
		'AttachmentController',
		[
			'only' => [
				'index',
				'show',
				'store',
				'update',
				'destroy',
			]
		]
	);

	Route::resource(
		'marker',
		'MarkerController',
		[
			'only' => [
				'index',
				'show',
				'store',
				'update',
				'destroy',
			]
		]
	);
});

// App Routes
Route::group(['middleware' => 'auth'], function () {
	Route::any('{path}', function () {
		$user = Auth::user();

		$data['user'] = (object)[
			'name' => $user->name,
			'image' => 'http://www.gravatar.com/avatar/' . md5( strtolower( trim($user->email) ) ) . '?d=mm'
		];
		return Response::view('app', $data);
	})->where('path', '[\w\d\/]*');
});