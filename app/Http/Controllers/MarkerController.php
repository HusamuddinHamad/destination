<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\MarkerFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Marker;
use App\Meta;


class MarkerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$search = \Request::get('search');
		$take = \Request::get('take') ?: 20;
		$paged = \Request::get('paged') ?: 1;
		$skip = $take * ($paged - 1) ;

		$result = Marker::orderBy('id', 'DESC');

		if ( !!$search )
			$result = $result->where('name', 'like', "%{$search}%");

		return \Response::json(
			[
				'count' 	=> $result->count(),
				'markers' 	=> 0 < (int)$take ? $result->skip($skip)->take($take)->get() : $result->get()
			]
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MarkerFormRequest $request)
	{

		$marker = new Marker( $request->all() );
		$marker->save();

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$marker = Marker::whereId($id)->with(['meta'])->first();

		return \Response::json(
			$marker
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MarkerFormRequest $request, $id)
	{

		$marker = Marker::find($id);
		$marker->update(
			$request->all()
		);

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($ids)
	{
		$ids = array_map(
			function ($el) {
				return (int)$el;
			},
			explode(',', trim($ids))
		);

		foreach ($ids as $id) {
			$marker = Marker::find($id)->delete();
		}

		return \Response::json([
			'success' => true
		]);
	}

}
