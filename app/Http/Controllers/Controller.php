<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;


	public function helpers () {
		$obj = new \stdClass;

		$obj->humanFileSize = function ($size, $unit="") {

			$units = ["Byte", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

			$size = (float)$size;
			$pow = intval(
				floor(
					log($size, 1024)
				)
			);

			return sprintf("%.2f %s", $size / pow(1024, $pow), $units[$pow]);

		};

		return $obj;
	}

}
