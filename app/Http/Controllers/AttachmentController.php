<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\AttachmentFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Attachment;
use App\Meta;


class AttachmentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$search = \Request::get('search');

		$project = \Request::get('project');
		$type = \Request::get('type');

		$take = \Request::get('take') ?: 20;
		$paged = \Request::get('paged') ?: 1;
		$skip = $take * ($paged - 1) ;

		$result = Attachment::orderBy('id', 'DESC');

		if ( !!$search )
			$result = $result->where('name', 'like', "%{$search}%");

		if ( !!$project && !!$type ) {
			$result->whereProjectId( $project )->whereType( $type );
		}

		return \Response::json(
			[
				'count' 	=> $result->count(),
				'attachments' 	=> 0 < (int)$take ? $result->skip($skip)->take($take)->get() : $result->get()
			]
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AttachmentFormRequest $request)
	{

		$attachment = new Attachment( $request->all() );
		$attachment->save();

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$attachment = Attachment::whereId($id)->with(['meta'])->first();

		return \Response::json(
			$attachment
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AttachmentFormRequest $request, $id)
	{

		$attachment = Attachment::find($id);
		$attachment->update(
			$request->all()
		);

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($ids)
	{
		$ids = array_map(
			function ($el) {
				return (int)$el;
			},
			explode(',', trim($ids))
		);

		foreach ($ids as $id) {
			$attachment = Attachment::find($id)->delete();
		}

		return \Response::json([
			'success' => true
		]);
	}

}
