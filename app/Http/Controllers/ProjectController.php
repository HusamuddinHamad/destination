<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ProjectFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Project;
use App\Meta;


class ProjectController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$search = \Request::get('search');
		$take = \Request::get('take') ?: 20;
		$paged = \Request::get('paged') ?: 1;
		$skip = $take * ($paged - 1) ;

		$result = Project::orderBy('id', 'DESC');

		if ( !!$search )
			$result = $result->where('name', 'like', "%{$search}%");

		return \Response::json(
			[
				'count' 	=> $result->count(),
				'projects' 	=> 0 < (int)$take ? $result->skip($skip)->take($take)->get() : $result->get()
			]
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProjectFormRequest $request)
	{

		$project = new Project([
			'name' => $request->get('name'),
		]);

		$project->save();
		$project->meta()->save(
			new Meta ([
				'key' 	=> 'connections',
				'value' => serialize( $request->only(['amazon', 'vuforia']) )
			])
		);

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projectQuery = Project::whereId($id)->with(['meta' => function ($query) {
			$query->where('key', '=', 'connections');
			$query->orderBy('id', 'DESC');
		}]);

		if( !!$projectQuery->count() )
			$project = $projectQuery->first()->toArray();
		else
			return null;

		$project['connections'] = isset($project['meta'][0]) ? $project['meta'][0]['value'] : null;
		unset( $project['meta'] );

		$project['connections'] = unserialize( $project['connections'] );

		return \Response::json(
			$project
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ProjectFormRequest $request, $id)
	{
		$project = Project::find($id);
		$project->name = $request->get('name');
		$project->save();

		$meta = $project->meta()->whereKey('connections')->orderBy('id', 'DESC');
		if ( !$meta->count() ) {
			$project->meta()->save(
				new Meta ([
					'key' 	=> 'connections',
					'value' => serialize( $request->only(['amazon', 'vuforia']) )
				])
			);
		} else {
			$meta->first()->update([
				'value' => serialize( $request->only(['amazon', 'vuforia']) )
			]);
		}

		return \Response::json([
			'success' => true
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($ids)
	{
		$ids = array_map(
			function ($el) {
				return (int)$el;
			},
			explode(',', trim($ids))
		);

		foreach ($ids as $id) {
			$project = Project::find($id)->delete();
		}

		return \Response::json([
			'success' => true
		]);
	}

}
