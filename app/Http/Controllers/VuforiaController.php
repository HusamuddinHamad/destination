<?php namespace App\Http\Controllers;

use \GuzzleHttp as HTTP;
use \App\Http\Helpers;

class VuforiaController extends Controller {
	private $access_key;
	private $secret_key;

	private $url 			= "https://vws.vuforia.com";
	private $requestPath 	= "/summary";
	private $data 			= null;
	private $request;

	public function __construct() {
		$this->access_key = env('VUFORIA_ACCESS_KEY');
		$this->secret_key = env('VUFORIA_SECRET_KEY');
	}

	public function getAllTargets() {

		// Define the Date and Authentication headers
		$date = new \DateTime("now", new \DateTimeZone("GMT"));
		$date = $date->format("D, d M Y H:i:s") . " GMT";

		$this->request = new HTTP\Psr7\Request(
			'GET',
			$this->url . $this->requestPath,
			[
				'Date' => $date,
			],
			$this->data
		);

		return $this->getResponse();
	}

	private function getResponse () {

		$client = new HTTP\Client();
		$sb = new Helpers\SignatureBuilder();

		$response = $client->send(
			$this->request,
			[
				'headers' => [
					'Authorization' => "VWS " . $this->access_key . ":" . $sb->tmsSignature( $this->request , $this->secret_key )
				]
			]
		);

		return $response->getBody();
	}

}