<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model {

	protected $table = 'markers';
	protected $fillable = ['name', 'url'];

	public function meta () {
		return $this->morphMany('App\Meta', 'metable');
	}

	/**
	 * Get the project that owns the marker
	 */
	public function project()
	{
	    return $this->belongsTo('App\Project');
	}


	public function attachments() {
		return $this->belongsToMany('App\Attachment');
	}

}