<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model {

	protected $table = 'attachments';
	protected $fillable = ['name', 'type', 'data'];

	public function meta () {
		return $this->morphMany('App\Meta', 'metable');
	}

	/**
	 * Get the project that owns the attachment
	 */
	public function project()
	{
	    return $this->belongsTo('App\Project');
	}

	public function markers() {
		return $this->belongsToMany('App\Marker');
	}

}