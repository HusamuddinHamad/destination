<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	protected $table = 'projects';
	protected $fillable = ['name'];

	public function meta () {
		return $this->morphMany('App\Meta', 'metable');
	}

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function markers()
    {
        return $this->hasMany('App\Marker');
    }

}