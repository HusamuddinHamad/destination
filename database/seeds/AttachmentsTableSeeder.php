<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Attachment;

class AttachmentsTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('attachments')->delete();

        $loremArr = ['Oratio', 'me', 'istius', 'philosophi', 'non', 'offendit', 'Aperiendum', 'est', 'igitur,', 'quid', 'sit', 'voluptas', 'Ergo,', 'inquit,', 'tibi', 'Q.', 'Duo', 'Reges:', 'constructio', 'interrete'];
        for( $i=0; $i<30; $i++ ) {
            $title = sprintf("%s %s %s", $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)]);

            $attachment = new Attachment([
                'name' => $title,

                'type' => ['video', 'audio', 'image'][ rand(0, 2) ],
                'data' => 'lorem ipsum',
                'project_id' => rand(1, 5)
            ]);

            $attachment->save();
        }
    }

}