<?php

use Illuminate\Database\Seeder;
use App\Marker;

class MarkersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('markers')->delete();

        $loremArr = ['Oratio', 'me', 'istius', 'philosophi', 'non', 'offendit', 'Aperiendum', 'est', 'igitur', 'quid', 'sit', 'voluptas', 'Ergo', 'inquit', 'tibi', 'Que', 'Duo', 'Reges', 'constructio', 'interrete'];
        for( $i=0; $i<30; $i++ ) {
			$title = sprintf("%s %s %s", $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)]);

	        $marker = new Marker([
	        	'name' => $title,
                'url'  => sprintf('//placehold.it/%dx%d', rand(300, 400), rand(300, 400)),
                'project_id' => rand(1, 5)
	    	]);

	    	$marker->save();

            $rand = range(1, 30);
            shuffle( $rand );

            $marker->attachments()->sync(
                array_slice($rand, rand(1, 20))
            );
        }
    }

}