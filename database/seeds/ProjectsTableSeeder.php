<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('projects')->delete();

        $loremArr = ['Oratio', 'me', 'istius', 'philosophi', 'non', 'offendit', 'Aperiendum', 'est', 'igitur,', 'quid', 'sit', 'voluptas', 'Ergo,', 'inquit,', 'tibi', 'Q.', 'Duo', 'Reges:', 'constructio', 'interrete'];
        for( $i=0; $i<5; $i++ ) {
			$title = sprintf("%s %s %s", $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)], $loremArr[rand(0, count($loremArr)-1)]);

	        $project = new Project([
	        	'name' => $title
	    	]);

	    	$project->save();
        }
    }

}