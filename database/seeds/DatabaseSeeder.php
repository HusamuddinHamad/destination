<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UsersTableSeeder');
		$this->command->info("Users table seeded");

		$this->call('ProjectsTableSeeder');
		$this->command->info("Projects table seeded");

		$this->call('AttachmentsTableSeeder');
		$this->command->info("Attachments table seeded");

		$this->call('MarkersTableSeeder');
		$this->command->info("Markers table seeded");
	}

}
