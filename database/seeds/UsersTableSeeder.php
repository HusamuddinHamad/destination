<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('users')->delete();
        $user = new User([
        	'name' 		=> 'demo',
        	'email' 	=> 'demo@example.com',
        	'password' => Hash::make('demo'),
    	]);

    	$user->save();
    }

}