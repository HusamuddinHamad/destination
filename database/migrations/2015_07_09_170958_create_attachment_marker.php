<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentMarker extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachment_marker', function(Blueprint $table)
		{
			$table->integer('marker_id')->unsigned()->index();
			$table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade');

			$table->integer('attachment_id')->unsigned()->index();
			$table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attachment_marker');
	}

}
