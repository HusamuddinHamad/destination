require(['app/config'], function() {
	require(['defining', 'run']);
});

define('defining', ['libs/angular'], function (angular) {
	angular.module("Resources", []);
	angular.module("Controllers", []);
	angular.module("Controllers.Routes", []);
	angular.module("Directives", []);
	angular.module("Routes", []);
});

define(
	'run',
	[
		'libs/angular',
		'libs/angular-route',
		'libs/angular-resource',

		'app/resources/Project',
		'app/resources/Attachment',
		'app/resources/Marker',

		'app/directives/dropdown',
		'app/directives/aside',
		'app/directives/tabs',
		'app/directives/table',
		'app/directives/table-flexible',

		'app/controllers/AppController',

		'app/controllers/routes/HomeController',

		'app/controllers/routes/ProjectsController',
		'app/controllers/routes/ProjectController',

		'app/controllers/routes/CategoriesController',
		'app/controllers/routes/CategoryController',

		'app/controllers/routes/MarkersController',
		'app/controllers/routes/MarkerController',

		'app/controllers/routes/AttachmentsController',
		'app/controllers/routes/AttachmentController',

		'app/routes',
	],
	function (angular) {

		angular.element(document).ready(function () {
			angular.module(
				'destination',
				[
					'ngRoute',
					'ngResource',

					'Resources',
					'Directives',
					'Controllers',
					'Controllers.Routes',
					'Routes',
				]
			);

			var app = angular.bootstrap(document, ['destination']);
		});
	}
);