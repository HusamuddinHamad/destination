define(function() {
    requirejs.config({
        waitSeconds: 120,
        paths: {
            "libs/jquery": "../libs/jquery/dist/jquery.min",
            "libs/angular": "../libs/angularjs/angular.min",
            "libs/angular-route": "../libs/angular-route/angular-route.min",
            "libs/angular-resource": "../libs/angular-resource/angular-resource.min",
            "libs/underscore": "../libs/underscore/underscore-min"
        },
        shim: {
            "libs/jquery": {
                exports: "$"
            },
            "libs/underscore": {
                exports: "_"
            },
            "libs/angular": {
                exports: "angular"
            },
            "libs/angular-route": {
                deps: ['libs/angular']
            },
            "libs/angular-resource": {
                deps: ['libs/angular']
            },
        }
    });

    require(['libs/jquery'], function ($) {
        $(document.body).on('click', 'a', function (e) {
            var href = $(this).attr('href');

            if ( !href || !href.trim().length || href.trim() === '#' )
                e.preventDefault();
        });

        $(document.body).on('click', '.disabled, .disabled a', function (e) {
            e.preventDefault();
            return false;
        });
    });
});
