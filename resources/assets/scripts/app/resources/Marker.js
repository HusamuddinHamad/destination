define(['libs/angular'], function (angular) {
	var Directive = angular.module("Resources");
	Directive.factory('Marker', function ($resource) {
		return $resource(
			'api/marker/:id',
			{id: '@id'},
			{
				query: {
					url: 'api/marker?take=:take&paged=:paged&search=:search',
					params: {
						take: '@take',
						paged: '@paged',
						search: '@search'
					},
					isArray: false,
				},
				store: {
					method: 'POST',
					isArray: false,
				},
				update: {
					method: 'PUT',
					isArray: false,
				},
				delete: {
					url: 'api/marker/:ids',
					method: 'DELETE',
					paras: {
						ids: '@ids'
					}
				}
			}
		);
	});
});