define(['libs/angular'], function (angular) {
	var Directive = angular.module("Resources");
	Directive.factory('Attachment', function ($resource) {
		return $resource(
			'api/attachment/:id',
			{id: '@id'},
			{
				query: {
					url: 'api/attachment?take=:take&paged=:paged&search=:search&project=:project&type=:type',
					params: {
						take: '@take',
						paged: '@paged',
						search: '@search',
						project: '@project',
						type: '@type',
					},
					isArray: false,
				},
				store: {
					method: 'POST',
					isArray: false,
				},
				update: {
					method: 'PUT',
					isArray: false,
				},
				delete: {
					url: 'api/attachment/:ids',
					method: 'DELETE',
					paras: {
						ids: '@ids'
					}
				}
			}
		);
	});
});