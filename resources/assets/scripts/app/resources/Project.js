define(['libs/angular'], function (angular) {
	var Directive = angular.module("Resources");
	Directive.factory('Project', function ($resource) {
		return $resource(
			'api/project/:id',
			{id: '@id'},
			{
				query: {
					url: 'api/project?take=:take&paged=:paged&search=:search',
					params: {
						take: '@take',
						paged: '@paged',
						search: '@search'
					},
					isArray: false,
				},
				store: {
					method: 'POST',
					isArray: false,
				},
				update: {
					method: 'PUT',
					isArray: false,
				},
				delete: {
					url: 'api/project/:ids',
					method: 'DELETE',
					paras: {
						ids: '@ids'
					}
				}
			}
		);
	});
});