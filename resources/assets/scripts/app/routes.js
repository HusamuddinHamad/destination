define(['libs/angular', 'libs/angular-route'], function() {
    Routes = angular.module('Routes');

    Routes.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                templateUrl: 'templates/home.html',
                controller: 'HomeController'
            })

            // project
            .when('/projects', {
                templateUrl: 'templates/projects.html',
                controller: 'ProjectsController'
            })
            .when('/projects/create', {
                templateUrl: 'templates/project.html',
                controller: 'ProjectController'
            })
            .when('/projects/:id', {
                templateUrl: 'templates/project.html',
                controller: 'ProjectController'
            })

            // attachment
            .when('/attachments', {
                templateUrl: 'templates/attachments.html',
                controller: 'AttachmentsController'
            })
            .when('/attachments/create', {
                templateUrl: 'templates/attachment.html',
                controller: 'AttachmentController'
            })
            .when('/attachments/:id', {
                templateUrl: 'templates/attachment.html',
                controller: 'AttachmentController'
            })

            // marker
            .when('/markers', {
                templateUrl: 'templates/markers.html',
                controller: 'MarkersController'
            })
            .when('/markers/create', {
                templateUrl: 'templates/marker.html',
                controller: 'MarkerController'
            })
            .when('/markers/:id', {
                templateUrl: 'templates/marker.html',
                controller: 'MarkerController'
            })


            // category
            .when('/categories', {
                templateUrl: 'templates/categories.html',
                controller: 'CategoriesController'
            })
            .when('/categories/create', {
                templateUrl: 'templates/category.html',
                controller: 'CategoryController'
            })

            // 404
            .otherwise({
                template: '<div class="a-table"> <div class="a-cell">404</div> </div>'
            });
    }]);
});