define(['libs/angular', 'libs/jquery'], function (angular, $) {

    var Directive = angular.module("Directives");
    Directive.directive("dropdown", function($window) {
        var dropdown = {
            restrict: "A",
            controller: function($scope) {
                $scope.userListIsActive = false;
                $scope.toggleActive = function() {
                    $scope.userListIsActive = !$scope.userListIsActive;
                };
                $scope.goNotActive = function() {
                    $scope.userListIsActive = false;
                };
            },
            link: function(scope, el, attrs) {
                var w = angular.element($window), that = el;
                w.bind("click", function(e) {
                    if (!$(e.target).closest(that).length) {
                        scope.goNotActive();
                        scope.$apply();
                    }
                });
            }
        };
        return dropdown;
    });

});