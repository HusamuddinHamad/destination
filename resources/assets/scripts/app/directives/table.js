define(['libs/angular', 'libs/jquery'], function (angular, $) {
    var Directive = angular.module("Directives");

    Directive.directive("table", [
        '$window',
        '$rootScope',
        'Project',
        'Attachment',
        'Marker',
        function($window, $rootScope, Project, Attachment, Marker) {
            var resources = {
                Project: Project,
                Attachment: Attachment,
                Marker: Marker
            };

            var directive = {
                restrict: "A",
                controller: function($scope) {
                },
                link: function(scope, el, attrs) {
                    var $el = $(el);

                    var checkAllButton = $el.find('#select-all');
                    var pagination = $el.find('.pagination');


                    checkAllButton.on('change', function () {
                        var btns = $el.find('input[type=checkbox]').not(checkAllButton);

                        if( $(this).prop('checked') ) {
                            btns.prop('checked', true);
                            scope.$emit(
                                'buttonsChecked',
                                (function () {
                                    var returnedArr = [];
                                    btns.each(function (i, el) {
                                        returnedArr.push(
                                            parseInt( $(el).val() )
                                        );
                                    });
                                    return returnedArr;
                                })()
                            );
                        } else {
                            btns.prop('checked', false);
                            scope.$emit('buttonsUnchecked');
                        }
                    });

                    $el.on('change', 'input[type=checkbox]:not(#select-all)', function () {
                        var
                            checkedButtons = [],
                            btns = $el.find('input[type=checkbox]').not(checkAllButton);

                        btns.each(function (i, btn) {
                            if( $(btn).prop('checked') === true )
                                checkedButtons.push( parseInt( $(btn).val() ) );
                        });

                        if ( btns.length === checkedButtons.length )
                            checkAllButton.prop('checked', true);
                        else
                            checkAllButton.prop('checked', false);

                        if ( !!checkedButtons.length )
                            scope.$emit('buttonsChecked', checkedButtons);
                        else
                            scope.$emit('buttonsUnchecked');
                    });

                    pagination.on('click', 'a', function (e) {
                        e.preventDefault();

                        var paged = $(this).attr('href').replace('#', '') || null;

                        // validate
                        if ( !paged ) return;

                        scope.$emit('loading');
                        $rootScope.$emit( 'paginate', {paged: paged} );
                    });

                    $rootScope.$on('paginated', function (d, data) {
                        var lis = pagination.find('li').not(':first').not(':last');
                        lis.eq( parseInt(data.paged) - 1 ).addClass('active').siblings('li').removeClass('active');
                        scope.$emit('done');
                    });

                    scope.$on('clear', function () {
                        checkAllButton.prop('checked', false);
                    });

                    scope.$on('delete', function (e, data) {
                        var
                            btns = $el.find('input[type=checkbox]').not(checkAllButton),
                            checkedButtons = [];

                        btns.each(function (i, btn) {
                            if( $(btn).prop('checked') === true )
                                checkedButtons.push( parseInt( $(btn).val() ) );
                        });

                        scope.$emit('loading');
                        resources[data.resource].delete({ids: checkedButtons.join(',')}, function (response) {
                            if (response.success === true) {
                                scope.$emit('reload');
                            }
                        });

                    });
                }
            };
            return directive;
        }
    ]);
});