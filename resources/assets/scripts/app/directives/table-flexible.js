define(['libs/angular', 'libs/jquery'], function (angular, $) {
    var Directive = angular.module("Directives");

    Directive.directive("tableFlexible", [
        '$window',
        function($window) {
            var directive = {
                restrict: "A",
                controller: function($scope) {
                },
                link: function(scope, el, attrs) {
                    var $el = $(el);

                    var checkAllButton = $el.find('input[type=checkbox]:first');
                    var pagination = $el.find('.pagination');

                    checkAllButton.on('change', function () {
                        var btns = $el.find('input[type=checkbox]').not(checkAllButton);

                        if( $(this).prop('checked') ) {
                            btns.prop('checked', true);
                            scope.$emit(
                                'buttonsChecked',
                                (function () {
                                    var returnedArr = [];
                                    btns.each(function (i, el) {
                                        returnedArr.push(
                                            parseInt( $(el).val() )
                                        );
                                    });
                                    return returnedArr;
                                })()
                            );
                        } else {
                            btns.prop('checked', false);
                        }
                    });

                    $el.on('change', 'input[type=checkbox]:not(#select-all)', function () {
                        var
                            checkedButtons = [],
                            btns = $el.find('input[type=checkbox]').not(checkAllButton);

                        btns.each(function (i, btn) {
                            if( $(btn).prop('checked') === true )
                                checkedButtons.push( parseInt( $(btn).val() ) );
                        });

                        if ( btns.length === checkedButtons.length )
                            checkAllButton.prop('checked', true);
                        else
                            checkAllButton.prop('checked', false);

                        if ( !!checkedButtons.length )
                            scope.$emit('buttonsChecked', checkedButtons);
                        else
                            scope.$emit('buttonsUnchecked');
                    });

                    scope.$on('clean', function () {
                        checkAllButton.prop('checked', false);
                    });
                }
            };
            return directive;
        }
    ]);
});