define(['libs/angular', 'libs/jquery'], function (angular, $) {

    var Directive = angular.module("Directives");
    Directive.directive("aside", function($window) {
        var aside = {
            restrict: "A",
            controller: function($scope) {
            },
            link: function(scope, el, attrs) {
                var $el = $(el);

                $el.find('.group > a').bind('click', function (e) {
                    e.preventDefault();
                    var group = $(this).parent();
                    var groupSiblings = $(this).parent().siblings('.group');

                    group.toggleClass('active');
                    groupSiblings.removeClass('active');

                    $el.find('.group.active ul').stop().slideDown();
                    $el.find('.group:not(.active) ul').stop().slideUp();

                });
            }
        };
        return aside;
    });
});