define(['libs/angular', 'libs/jquery'], function (angular, $) {

    var Directive = angular.module("Directives");
    Directive.directive("tabs", function($window) {
        var tabs = {
            restrict: "A",
            controller: function($scope) {
            },
            link: function(scope, el, attrs) {
                var $el = $(el);

                $el.find('.nav a').on('click', function (e) {
                    e.preventDefault();

                    var href = $(this).attr('href');

                    // validate
                    if( !href )
                        return;

                    var pane = $el.find('.tab-content ' + href);

                    // validate
                    if ( !pane )
                        return;

                    $(this).parent('li').addClass('active').siblings('li').removeClass('active');
                    pane.addClass('active').siblings().removeClass('active');

                });
            }
        };
        return tabs;
    });
});