define(['libs/angular'], function(angular) {
    var Controllers = angular.module("Controllers");

    Controllers.controller('AppController', ['$scope', '$rootScope', '$location', '$route', function($scope, $rootScope, $location, $route) {
        $scope.activeAside = true;
        $scope.entireLoading = false;
        $rootScope.breadcrumps = [];

        $scope.toggleSide = function() {
            $scope.activeAside = !$scope.activeAside;
        };

        $scope.$on('$locationChangeStart', function () {
            $scope.navLoading = true && !$scope.entireLoading;
        });

        $scope.$on('$locationChangeSuccess', function () {
            $scope.path = $location.$$path;
            $scope.navLoading = false;
        });

        $scope.$on('reload', function () {
            $route.reload();
        });
    }]);
});
