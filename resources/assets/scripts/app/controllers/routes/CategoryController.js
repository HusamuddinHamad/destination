define(['libs/angular'], function (angular) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('CategoryController', function ($rootScope, $scope) {
    	$rootScope.breadcrumps = [
    		{
    			name: 'Categories',
    			link: 'categories'
    		},
    		{
    			name: 'Create Category',
    			link: 'categories/create'
    		}
    	];
    });
});