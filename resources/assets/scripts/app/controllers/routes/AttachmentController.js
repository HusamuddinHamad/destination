define(['libs/angular'], function(angular) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('AttachmentController', [
        '$scope',
        '$rootScope',
        '$routeParams',
        'Attachment',
        '$route',
        '$location',
        function($scope, $rootScope, $routeParams, Attachment, $route, $location) {
            var id = $routeParams.id || null;
            var action = 'create';

            $scope.report = {};

            setBreadcrumps = function() {
                $rootScope.breadcrumps = [{
                    name: 'Attachments',
                    link: 'attachments'
                }, {
                    name: $scope.id ? 'Update Attachment' : 'Create Attachment',
                    link: '#'
                }];
            }; setBreadcrumps();

            if(!isNaN(id) && !!id) {
                Attachment.get({id: id}, function (data) {
                    $scope.id = data.id;
                    $scope.name = data.name;

                    setBreadcrumps();
                    $scope.$broadcast('done');
                });
                action = 'update';
            }

            $scope.setAction = function (actionType) {
                action = actionType || action;
            };

            $scope.save = function () {

                function cb (status, feeds) {

                    $scope.report.status = status;
                    $scope.report.feeds = feeds;

                    $scope.report.el = {
                        class: ($scope.report.status === 'success') ? 'alert-success' : 'alert-danger'
                    };
                    $scope.$broadcast('done');
                }

                if ( action === 'create' ) {
                    $scope.$broadcast('loading');
                    Attachment.store(
                        {
                            name: $scope.name || ''
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been created success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );

                } else if ( action === 'update' ) {
                    $scope.$broadcast('loading');
                    Attachment.update(
                        {
                            id: $scope.id,
                            name: $scope.name || ''
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been updated success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );
                } else if ( action === 'delete' ) {

                    var confirmation = confirm('Are you sure that you want to delete?!');
                    if ( !confirmation )
                        return;

                    $scope.$broadcast('loading');
                    Attachment.delete(
                        {
                            ids: $scope.id
                        },
                        function (response) {
                            $location.path('markers');
                        }
                    );
                }

            };

            $scope.$on('loading', function () {
                $scope.loading = true;
            });

            $scope.$on('done', function () {
                $scope.loading = false;
            });

        }
    ]);
});
