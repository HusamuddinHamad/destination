define(['libs/angular', 'libs/underscore'], function (angular, _) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('ProjectsController', [
    	'Project',
    	'$scope',
    	'$rootScope',
    	'$route',
    	function (Project, $scope, $rootScope, $route) {
    		var
    			take = 10,
    			timeout = 1 * 1000;

    		$scope.paged = 1;
    		$scope.maxNumberOfpages = 1;
    		$scope.loading = true;

	    	$rootScope.breadcrumps = [
	    		{
	    			name: 'Projects',
	    			link: 'projects'
	    		}
	    	];

	    	$scope.clearSearchQuery = function () {
	    		$scope.searchQuery = "";
	    	};

    		var queryAction = function (take, paged) {
    			var passedData = {
					take: take,
					paged: paged || 1,
					search: $scope.searchQuery || ''
				};

    			Project.query(passedData, function (data) {
	    			$scope.projects = data.projects;

		    		$scope.maxNumberOfpages = Math.ceil( data.count / take );
		    		$scope.numberOfpages = _.range(1, $scope.maxNumberOfpages + 1);

		    		$scope.paged = paged;
		    		$rootScope.$emit('paginated', {paged: paged});
		    		$scope.$broadcast('clear');
		    	});
    		}; // set the default query

	    	$scope.delete = function () {
	    		var confirmed = confirm('You are about to delete these projects, and you will not be able to restore them.');
	    		if ( confirmed )
	    			$scope.$broadcast('delete', {resource: 'Project'});
	    	};

	    	queryAction(take, 1); // initialize the query

	    	$rootScope.$on('paginate', function (event, data) {
	    		var paged = parseInt(data.paged) || 1;
	    		queryAction(take, paged);
	    	});

	    	$scope.$on('loading', function () {
	    		$scope.loading = true;
	    	});

	    	$scope.$on('done', function () {
	    		$scope.loading = false;
	    	});

	    	$scope.$on('buttonsChecked', function (e, itemToDelete) {
	    		$scope.itemToDelete = itemToDelete;
	    	});

	    	$scope.$on('buttonsUnchecked', function () {
				$scope.itemToDelete = null;
	    	});

	    	var searchAction = _.debounce(function (query) {
	    		// validate
	    		if ( !query ) {
			    	queryAction(take, 1);
	    		} else {
			    	queryAction(take, 1, query);
	    		}

	    	}, timeout);


	    	var preSearchAction = function (value) {
	    		if ( !value ) {
	    			searchAction(null);
	    			return false;
	    		}

	    		$scope.$broadcast('loading');
	    		searchAction( value );
	    	};

	    	$scope.searchSubmit = function () {
	    		preSearchAction( $scope.searchQuery );
	    	};

	    	$scope.$watch('searchQuery', function (newValue, oldValue) {

	    		// validate
	    		if ( newValue === oldValue )
		    		return;

		    	preSearchAction(newValue);
	    	});

	    }
    ]);
});