define(['libs/angular'], function (angular) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('CategoriesController', function ($rootScope, $scope) {
    	$rootScope.breadcrumps = [
    		{
    			name: 'Categories',
    			link: 'categories'
    		}
    	];
    });
});