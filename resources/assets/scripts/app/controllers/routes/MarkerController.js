define(['libs/angular', 'libs/jquery', 'libs/underscore'], function (angular, $, _) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('MarkerController', [
        '$scope',
        '$rootScope',
        '$routeParams',
        'Marker',
        '$route',
        '$location',
        'Project',
        'Attachment',
        function ($scope, $rootScope, $routeParams, Marker, $route, $location, Project, Attachment) {

            var id = $routeParams.id || null;
            var action = 'create';

            $scope.report = {};

            setBreadcrumps = function() {
                $rootScope.breadcrumps = [{
                    name: 'Markers',
                    link: 'markers'
                }, {
                    name: $scope.id ? 'Update Marker' : 'Create Marker',
                    link: '#'
                }];
            }; setBreadcrumps();

            if(!isNaN(id) && !!id) {
                Marker.get({id: id}, function (data) {
                    $scope.id = data.id;
                    $scope.name = data.name;

                    setBreadcrumps();
                    $scope.$broadcast('done');
                });
                action = 'update';
            }

            $scope.setAction = function (actionType) {
                action = actionType || action;
            };

            $scope.save = function () {

                function cb (status, feeds) {

                    $scope.report.status = status;
                    $scope.report.feeds = feeds;

                    $scope.report.el = {
                        class: ($scope.report.status === 'success') ? 'alert-success' : 'alert-danger'
                    };
                    $scope.$broadcast('done');
                }

                if ( action === 'create' ) {
                    $scope.$broadcast('loading');

                    console.log(
                        new FormData( $scope.theForm )
                    );

                    Marker.store(
                        {
                            name: $scope.name || '',
                            type: $scope.type,
                            project: $scope.project,
                            attachments: $scope.attachments,
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been created success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );

                } else if ( action === 'update' ) {
                    $scope.$broadcast('loading');
                    Marker.update(
                        {
                            id: $scope.id,
                            name: $scope.name || ''
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been updated success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );
                } else if ( action === 'delete' ) {

                    var confirmation = confirm('Are you sure that you want to delete?!');
                    if ( !confirmation )
                        return;

                    $scope.$broadcast('loading');
                    Marker.delete(
                        {
                            ids: $scope.id
                        },
                        function (response) {
                            $location.path('markers');
                        }
                    );
                }
            };

            $scope.$on('loading', function () {
                $scope.loading = true;
            });

            $scope.$on('done', function () {
                $scope.loading = false;
            });

            $scope.type = '';
            $scope.$watch('type', function (newValue, oldValue) {
                if( newValue === oldValue )
                    return;

                $scope.$broadcast('filterAttachments');
            });

            $scope.project = '';
            $scope.$watch('project', function (newValue, oldValue) {
                if( newValue === oldValue )
                    return;

                $scope.$broadcast('filterAttachments');
            });


            $scope.$on('filterAttachments', function () {

                if ( !$scope.type || !$scope.project )
                    return;

                $scope.$broadcast('loading');
                $scope.$broadcast('clean');
                Attachment.query(
                    {
                        take: -1,
                        type: $scope.type,
                        project: $scope.project,
                        attachments: $scope.attachments,
                    },
                    function (data) {
                        $scope.attachments = data.attachments;
                        $scope.$broadcast('done');
                    }
                );
            });

            Project.query({take: -1}, function (data) {
                $scope.projects = data.projects;

                if ( !$scope.project )
                    _.delay(function () {
                        $('[ng-model="project"]').prop('value', '');
                    }, 200);
            });

        }
    ]);
});