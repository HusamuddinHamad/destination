define(['libs/angular'], function (angular) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('HomeController', function ($scope, $rootScope) {
    	$rootScope.breadcrumps = [];
    });
});