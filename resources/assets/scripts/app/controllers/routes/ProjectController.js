define(['libs/angular'], function (angular) {
    var Controllers = angular.module("Controllers.Routes");

    Controllers.controller('ProjectController', [
        '$scope',
        '$rootScope',
        '$routeParams',
        'Project',
        '$route',
        '$location',
        function ($scope, $rootScope, $routeParams, Project, $route, $location) {
            var id = $routeParams.id || null;
            var action = 'create';

            $scope.report = {};

            setBreadcrumps = function () {
                $rootScope.breadcrumps = [
                    {
                        name: 'Projects',
                        link: 'projects'
                    },
                    {
                        name: $scope.id ? 'Update Project' : 'Create Project',
                        link: '#'
                    }
                ];
            }; setBreadcrumps();

            if(!isNaN(id) && !!id) {
                Project.get({id: id}, function (data) {
                    $scope.id = data.id;
                    $scope.name = data.name;

                    $scope.amazon = data.connections.amazon || {};
                    $scope.vuforia = data.connections.vuforia || {};

                    setBreadcrumps();
                    $scope.$broadcast('done');
                });
                action = 'update';
            }


            $scope.setAction = function (actionType) {
                action = actionType || action;
            };

            $scope.save = function () {

                function cb (status, feeds) {

                    $scope.report.status = status;
                    $scope.report.feeds = feeds;

                    $scope.report.el = {
                        class: ($scope.report.status === 'success') ? 'alert-success' : 'alert-danger'
                    };
                    $scope.$broadcast('done');
                }

                if ( action === 'create' ) {
                    $scope.$broadcast('loading');
                    Project.store(
                        {
                            name: $scope.name || '',
                            amazon: $scope.amazon,
                            vuforia: $scope.vuforia
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been created success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );

                } else if ( action === 'update' ) {
                    $scope.$broadcast('loading');
                    Project.update(
                        {
                            id: $scope.id,
                            name: $scope.name || '',
                            amazon: $scope.amazon,
                            vuforia: $scope.vuforia
                        },
                        function (response) {
                            if ( !!response.success ) {
                                cb(
                                    'success',
                                    ['The project has been updated success']
                                );
                            } else {
                                cb(
                                    'fail',
                                    response.messages
                                );
                            }
                        }
                    );
                } else if ( action === 'delete' ) {

                    var confirmation = confirm('Are you sure that you want to delete?!');
                    if ( !confirmation )
                        return;

                    $scope.$broadcast('loading');
                    Project.delete(
                        {
                            ids: $scope.id
                        },
                        function (response) {

                            $location.path('projects');
                        }
                    );
                }

            };

            $scope.$on('loading', function () {
                $scope.loading = true;
            });

            $scope.$on('done', function () {
                $scope.loading = false;
            });
        }
    ]);
});