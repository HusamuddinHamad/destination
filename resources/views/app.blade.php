<!DOCTYPE html>
<html lang="en" ng-controller="AppController">
	<head>
		<base href="/">

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">


		<title ng-bind='"Destination " + path'>Destination</title>

		<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	</head>
	<body class="app">
		<div ng-class="{hidden: !entireLoading}" class="loading">
			<div class="a-table">
				<div class="a-cell">
					<i class="fa fa-spinner fa-pulse"></i>
				</div>
			</div>
		</div>
		<header>
			<div class="container-fluid">
				<a id="toggle-menu" ng-click="toggleSide()">
					<i class="fa fa-bars"></i>
					<i ng-class="{hidden: !activeAside}" class="fa fa-minus-square"></i>
					<i ng-class="{hidden: activeAside}" class="fa fa-plus-square hidden"></i>
				</a>
				<ul class="breadcrumps horizontal pull-left" href="{{ URL::to('/') }}">
					<li>
						<a href="{{ URL::to('/') }}">
							Destination
						</a>
					</li>
					<li ng-repeat="page in breadcrumps">
						<a href="@{{ page.link }}">
							@{{ page.name }}
						</a>
					</li>
				</ul>
				<div dropdown ng-class="{ active: userListIsActive }" class="user pull-right">
					<a  ng-click="toggleActive()">
						<div class="pull-left">
							{{ $user->name }}
						</div>
						<div class="pull-left icon">
							<img src="{{ $user->image }}" alt="{{ $user->name }}">
						</div>
						<div class="pull-right">
							<i class="fa fa-caret-down"></i>
						</div>
					</a>
					<ul class="menu">
						<li>
							<a href="{{ URL::to('user/logout') }}" target="_self">
								<i class="fa fa-sign-out"></i>
								Logout
							</a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</header>
		<div class="wrap">
			<aside aside ng-class="{active: activeAside}" class="wrap-aside active">
				<nav>
					<div class="group">
						<a href="#">
							<i class="fa fa-briefcase"></i>
							Projects
						</a>
						<ul class="mere-list">
							<li>
								<a href="projects">All Projects</a>
							</li>
							<li>
								<a href="projects/create">Create Project</a>
							</li>
						</ul>
					</div>

					<div class="group">
						<a href="#">
							<i class="fa fa-bookmark"></i>
							Markers
						</a>
						<ul class="mere-list">
							<li>
								<a href="markers">All Markers</a>
							</li>
							<li>
								<a href="markers/create">Create Marker</a>
							</li>
						</ul>
					</div>

					<div class="group">
						<a href="#">
							<i class="fa fa-paperclip"></i>
							Attachments
						</a>
						<ul class="mere-list">
							<li>
								<a href="attachments">All Attachments</a>
							</li>
							<li>
								<a href="attachments/create">Create attachment</a>
							</li>
						</ul>
					</div>

					<div class="group">
						<a href="#">
							<i class="fa fa-flag"></i>
							Categories
						</a>
						<ul class="mere-list">
							<li>
								<a href="categories">All Categories</a>
							</li>
							<li>
								<a href="categories/create">Create Category</a>
							</li>
						</ul>
					</div>
				</nav>
			</aside>
			<div class="wrap-content">
				<div class="container-fluid" ng-view>
					@yield('content')
				</div>
			</div>
			<div ng-class="{hidden: !navLoading}" class="loading hidden">
				<div class="a-table">
					<div class="a-cell">
						<i class="fa fa-spinner fa-pulse"></i>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('libs/requirejs/require.js') }}" data-main="{{ asset('js/index.js') }}"></script>
	</body>
</html>