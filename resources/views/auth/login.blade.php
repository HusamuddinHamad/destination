@extends('plain')

@section('content')

<div class="login-template">
	<div class="a-table">
		<div class="a-cell">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a class="logo pull-left" href="{{ URL::to('/') }}">
						Destination
					</a>
					<div class="clearfix"></div>
				</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-xs-12">
								<input autofocus type="text" placeholder="Username / Email" class="form-control input-lg" name="user" value="{{ old('user') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-xs-8">
								<input type="password" placeholder="Password" class="form-control" name="password">
							</div>
							<div class="col-xs-4">
								<button type="submit" class="btn btn-primary btn-block">Login</button>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="form-group">
							<div class="col-xs-6">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
							<div class="col-xs-6">
								<a class="btn btn-link btn-block" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




@endsection
